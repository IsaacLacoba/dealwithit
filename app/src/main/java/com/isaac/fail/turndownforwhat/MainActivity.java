package com.isaac.fail.turndownforwhat;


import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "DealWithItActivity";
    private static final int startAt1stSecond = 1300;
    private MediaPlayer mediaPlayer;
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVideoView();
        setMediaPlayer();
        showAboutInfo();
    }

    private void showAboutInfo() {
        Snackbar.make(findViewById(R.id.baseLayout), getResources().getString(R.string.about_author), Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN)
            playMusic();
        return super.onTouchEvent(event);
    }

    private void setMediaPlayer() {
        mediaPlayer = MediaPlayer.create(getBaseContext(), R.raw.deal_with_it);
        mediaPlayer.seekTo(startAt1stSecond);
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mediaPlayer.seekTo(startAt1stSecond);
            }
        });
    }

    private void setVideoView() {
        videoView = (VideoView) findViewById(R.id.videoView);

        Uri video = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.deal_with_snoop);
        videoView.setVideoURI(video);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.setLooping(true);
            }
        });
        videoView.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            videoView.pause();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (mediaPlayer.getCurrentPosition() != startAt1stSecond)
            mediaPlayer.start();
        videoView.start();
    }

    private void playMusic() {
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.pause();
            return;
        }

        mediaPlayer.seekTo(startAt1stSecond);
        mediaPlayer.start();
        videoView.start();
    }
    public void playMusic(View view) {
        Log.e(TAG, "playMusic()");
        playMusic();
    }

}
